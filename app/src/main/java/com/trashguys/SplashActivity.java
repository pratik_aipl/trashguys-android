package com.trashguys;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.trashguys.Utility.Constant;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        int SPLASH_TIME_OUT = 3500;
        new Handler().postDelayed(() -> {
            if (prefs.getBoolean(Constant.isLogin, false)) {
                Intent i = new Intent(this, ScheduleActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            } else {
                Intent i = new Intent(this, LoginActivity.class);
                startActivity(i);
            }
            finish();
        }, SPLASH_TIME_OUT);

    }
}
