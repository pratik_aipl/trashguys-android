package com.trashguys;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.onesignal.OneSignal;
import com.trashguys.Utility.Prefs;

import java.io.File;
import java.util.Objects;


public class TrashApplication extends Application {

    public static Context mContext;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = this;
        Prefs.with(this);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                //.setNotificationReceivedHandler(new NotificationReceivedHandler())
                // .setNotificationOpenedHandler(new NotificationOpenedHandler(getApplicationContext()))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    @Override
    public void onTerminate() {
        android.os.Process.killProcess(android.os.Process.myPid());
        SharedPreferences.Editor editor = getSharedPreferences("clear_cache", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
        trimCache(this);
        super.onTerminate();
    }


    private static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);

            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                boolean success = deleteDir(new File(dir, child));
                if (!success) {
                    return false;
                }
            }
        }
        return Objects.requireNonNull(dir).delete();
    }

}
