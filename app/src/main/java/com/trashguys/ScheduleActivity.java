package com.trashguys;

import android.app.AlertDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.trashguys.Utility.Constant;
import com.trashguys.Utility.L;
import com.trashguys.network.NetworkRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ScheduleActivity extends BaseActivity {

    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    private Subscription subscription;

    @BindView(R.id.tv_bulkdate)
    TextView tvBulkdate;
    @BindView(R.id.tv_sun)
    TextView tvSun;
    @BindView(R.id.img_sun)
    ImageView imgSun;
    @BindView(R.id.tv_mon)
    TextView tvMon;
    @BindView(R.id.img_mon)
    ImageView imgMon;
    @BindView(R.id.tv_tue)
    TextView tvTue;
    @BindView(R.id.img_tue)
    ImageView imgTue;
    @BindView(R.id.tv_wed)
    TextView tvWed;
    @BindView(R.id.img_wed)
    ImageView imgWed;
    @BindView(R.id.tv_thu)
    TextView tvThu;
    @BindView(R.id.img_thu)
    ImageView imgThu;
    @BindView(R.id.tv_fri)
    TextView tvFri;
    @BindView(R.id.img_fri)
    ImageView imgFri;
    @BindView(R.id.tv_sat)
    TextView tvSat;
    @BindView(R.id.img_sat)
    ImageView imgSat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        ButterKnife.bind(this);

        if (L.isNetworkAvailable(ScheduleActivity.this))
            getScheduleList();

    }

    @OnClick(R.id.btn_logout)
    public void onViewClicked() {
        alert();
    }

    private void getScheduleList() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getSchedule(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if (jsonResponse.has(Constant.data)) {

                        JSONObject jsonObject = jsonResponse.getJSONObject(Constant.data);
                        tvBulkdate.setText("From " + jsonObject.getString("BulkPickupDate") + " To " + jsonObject.getString("BulkPickupEndDate"));
                        tvUsername.setText(jsonObject.getString("ManagerName"));
                        tvAddress.setText("Address: " + jsonObject.getString("Address"));

                        if (jsonObject.getString("isSun").equalsIgnoreCase("1"))
                            imgSun.setImageResource(R.drawable.right);
                        if (jsonObject.getString("isMon").equalsIgnoreCase("1"))
                            imgMon.setImageResource(R.drawable.right);
                        if (jsonObject.getString("isTue").equalsIgnoreCase("1"))
                            imgTue.setImageResource(R.drawable.right);
                        if (jsonObject.getString("isWed").equalsIgnoreCase("1"))
                            imgWed.setImageResource(R.drawable.right);
                        if (jsonObject.getString("isThu").equalsIgnoreCase("1"))
                            imgThu.setImageResource(R.drawable.right);
                        if (jsonObject.getString("isFri").equalsIgnoreCase("1"))
                            imgFri.setImageResource(R.drawable.right);
                        if (jsonObject.getString("isSat").equalsIgnoreCase("1"))
                            imgSat.setImageResource(R.drawable.right);


                        /*if (!jsonObject.getString("SunPickupTime").equalsIgnoreCase(""))
                            tvSun.setText("Sunday" + " (" + jsonObject.getString("SunPickupTime") + ")");
                        if (!jsonObject.getString("MonPickupTime").equalsIgnoreCase(""))
                            tvMon.setText("Monday" + " (" + jsonObject.getString("MonPickupTime") + ")");
                        if (!jsonObject.getString("TuePickupTime").equalsIgnoreCase(""))
                            tvTue.setText("Tuesday" + " (" + jsonObject.getString("TuePickupTime") + ")");
                        if (!jsonObject.getString("WedPickupTime").equalsIgnoreCase(""))
                            tvWed.setText("Wednesday" + " (" + jsonObject.getString("WedPickupTime") + ")");
                        if (!jsonObject.getString("ThuPickupTime").equalsIgnoreCase(""))
                            tvThu.setText("Thursday" + " (" + jsonObject.getString("ThuPickupTime") + ")");
                        if (!jsonObject.getString("FriPickupTime").equalsIgnoreCase(""))
                            tvFri.setText("Friday" + " (" + jsonObject.getString("FriPickupTime") + ")");
                        if (!jsonObject.getString("SatPickupTime").equalsIgnoreCase(""))
                            tvSat.setText("Saturday" + " (" + jsonObject.getString("SatPickupTime") + ")");*/

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    public void onBackPressed() {
        exitalert();
    }

    private void alert() {
        new AlertDialog.Builder(this)
                .setMessage((R.string.logout))
                .setPositiveButton("YES", (dialog, which) -> L.logout(this))
                .setNegativeButton("NO", null)
                .show();
    }

    private void exitalert() {
        new AlertDialog.Builder(this)
                .setMessage(("Are you sure you want to exit ?"))
                .setPositiveButton("YES", (dialog, which) -> finish())
                .setNegativeButton("NO", null)
                .show();
    }

    @Override
    protected void onDestroy() {
        if (subscription !=null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
            subscription=null;
        }
        super.onDestroy();
    }
}
