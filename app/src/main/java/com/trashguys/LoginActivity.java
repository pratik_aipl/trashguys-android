package com.trashguys;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.trashguys.Utility.Constant;
import com.trashguys.Utility.L;
import com.trashguys.network.NetworkRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.input_userid)
    EditText inputUserid;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
        if (TextUtils.isEmpty(L.getEditText(inputUserid))) {
            inputUserid.setError("Please enter valid community code");
        } else if (TextUtils.isEmpty(L.getEditText(inputPassword))) {
            inputPassword.setError("Please enter Password");
        } else {
            if (L.isNetworkAvailable(LoginActivity.this))
                callLogin();
        }
    }


    private void callLogin() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.login, L.getEditText(inputUserid));
        map.put(Constant.password, L.getEditText(inputPassword));
        map.put(Constant.DeviceID, L.getDeviceId(this));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogin(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    prefs.save(Constant.loginAuthToken, DATA.getString("login_token"));
                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    prefs.save(Constant.isLogin, true);

                    Intent intent = new Intent(this, ScheduleActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    protected void onDestroy() {
        if (subscription !=null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
            subscription=null;
        }
        super.onDestroy();
    }
}
