package com.trashguys.Utility;

public class Constant {


    public static final String UserData = "UserData";

    public static boolean isAlertShow = false;
    public static final String message = "message";
    public static final String isLogin = "isLogin";
    public static final String loginAuthToken = "loginAuthToken";
    public static final String data = "data";
    public static final String password = "password";
    public static final String login = "login";
    public static final String DeviceID = "device_id";

}
