package com.trashguys.Utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Alejandro Rodriguez <https://github.com/Alexrs95/Prefs>
 *         <p/>
 *         Wrapper over the Android Preferences which provides a fluid syntax
 */
public class Prefs {

    private static final String TAG = "Prefs";

    private static Prefs singleton = null;

    private static SharedPreferences preferences;

    private static SharedPreferences.Editor editor;

    private Prefs(Context context) {
        preferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static Prefs with(Context context) {
        if (singleton == null) {
            singleton = new Builder(context).build();
        }
        return singleton;
    }

    public void save(String key, boolean value) {
        editor.putBoolean(key, value).apply();
    }

    public void save(String key, String value) {
        editor.putString(key, value).commit();
        editor.putString(key, value).apply();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    public String getString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    private static class Builder {

        private final Context context;

        Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.context = context;
        }

        /**
         * Method that creates an instance of Prefs
         *
         * @return an instance of Prefs
         */
        Prefs build() {
            return new Prefs(context);
        }
    }

}
