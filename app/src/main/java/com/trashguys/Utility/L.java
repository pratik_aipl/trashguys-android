package com.trashguys.Utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.widget.EditText;

import com.google.gson.Gson;
import com.trashguys.DialogButtonListener;
import com.trashguys.LoginActivity;
import com.trashguys.R;
import com.trashguys.UserData;

import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Response;

public class L {

    private static boolean isShowing = false;

    //Get Device Id
    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

    }

    //For Network Available
    public static boolean isNetworkAvailable(Context context) {
        boolean isConnected = false;
        if (!Constant.isAlertShow) {
            if (!isNetworkConnected(context)) {
                showTwoButtonDialog(context, context.getString(R.string.network_error_title), context.getString(R.string.network_connection_error), "Ok", null, new DialogButtonListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        Constant.isAlertShow = false;
                    }

                    @Override
                    public void onNegativButtonClicked() {
                        Constant.isAlertShow = false;
                    }
                });
                Constant.isAlertShow = true;
                isConnected = false;
            } else
                isConnected = true;
        }
        return isConnected;
    }

    private static boolean isNetworkConnected(Context c) {
        boolean isConnected = false;



        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            switch (activeNetwork.getType()) {
                case ConnectivityManager.TYPE_WIFI:
                    // connected to wifi
                    isConnected = true;
                    break;
                case ConnectivityManager.TYPE_MOBILE:
                    // connected to mobile data
                    isConnected = true;
                    break;
                default:
                    break;
            }
        } else {
            return false;
        }
        return isConnected;
    }

    private static void showTwoButtonDialog(Context context, String title, String message, String yesButtonName, String noButtonName, DialogButtonListener dialogButtonListener) {

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        if (title != null)
            alertDialogBuilder.setTitle(title);
        else {
            alertDialogBuilder.setTitle("");
        }
        alertDialogBuilder
                .setMessage(Html.fromHtml(message))
                .setCancelable(false);
        if (yesButtonName != null) {
            yesButtonName = yesButtonName.equals("") ? "YES" : yesButtonName;
            alertDialogBuilder.setPositiveButton(yesButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onPositiveButtonClicked();
                }
            });
        }

        if (noButtonName != null) {
            noButtonName = noButtonName.equals("") ? "NO" : noButtonName;
            alertDialogBuilder.setNegativeButton(noButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onNegativButtonClicked();
                }
            });
        }
        // create alert dialog
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public static void serviceStatusFalseProcess(Context context, Response<String> data) {
        try {
           /* if (data.code() == 401) {
                L.logout(context);
            } else */
            if (data.code() == 500) {
                L.generalOkAlert(context, data.message(), data);
            } else {
                String msg;
                String str = Objects.requireNonNull(data.errorBody()).string();
                JSONObject jsonResponse = new JSONObject(str);
//                if (jsonResponse.has(Constant.messageAr) && L.isArabicLanguage(context)) {
//                    msg = jsonResponse.getString(Constant.messageAr);
//                } else {
                msg = jsonResponse.getString(Constant.message);

                L.generalOkAlert(context, msg, data);
            }

        } catch (Exception e) {
            e.printStackTrace();
            L.generalOkAlert(context, "oops!\nsomething went wrong.\nPlease try again!", null);
            e.printStackTrace();
        }
    }

    private static void generalOkAlert(Context context, String message, Response<String> data) {
        if (context == null)
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton(context.getResources().getString(R.string.ok), (dialog, which) -> {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                dialog.dismiss();
                isShowing = false;
                if (data != null && data.code() == 401)
                    L.logout(context);
            }
        });
        if (!isShowing) {
            builder.show();
            isShowing = true;
        }

    }

    public static void logout(Context context) {
        Prefs prefs = Prefs.with(context);
        prefs.save(Constant.isLogin, false);
        prefs.save(Constant.UserData, null);

        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static String getAuthtoken(Context context) {
        return Prefs.with(context).getString(Constant.loginAuthToken, "");
    }
    public static UserData getUser(Context context) {
        return new Gson().fromJson(Prefs.with(context).getString(Constant.UserData, ""), UserData.class);
    }

    public static boolean isLogin(Context context) {
        return Prefs.with(context).getBoolean(Constant.isLogin, false);
    }

    public static String getEditText(EditText editText) {
        return editText.getText().toString().trim();
    }
}
