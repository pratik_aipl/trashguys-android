package com.trashguys.network;

import java.util.Map;

import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

public interface RestApi {


    @FormUrlEncoded
    @POST("login")
    Observable<Response<String>> getLogin(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("schedule")
    Observable<Response<String>> getSchedule(@FieldMap Map<String, String> stringMap);

}
