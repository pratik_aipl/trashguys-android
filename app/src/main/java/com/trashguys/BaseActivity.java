package com.trashguys;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.trashguys.Utility.Prefs;
import com.trashguys.network.RestAPIBuilder;
import com.trashguys.network.RestApi;

public class BaseActivity extends AppCompatActivity {

    RestApi restApi;
    Prefs prefs;
    private ProgressDialog progressDialog;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restApi = RestAPIBuilder.buildRetrofitService();
        prefs = Prefs.with(this);

    }

    void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);

            progressDialog.setMessage("Loading...");
        }
        if (isShow && !progressDialog.isShowing()) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}
