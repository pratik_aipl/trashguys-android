package com.trashguys;

public interface DialogButtonListener {
    void onPositiveButtonClicked();
    void onNegativButtonClicked();
}